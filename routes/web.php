<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SleepQuality@index')->name('home');
Route::post('/', 'SleepQuality@testStore')->name('testStore');

Route::get('/profile' , 'ProfileController@index')->middleware('auth')->name('profile');


// Authentication Routes
Route::group(['namespace' => 'Auth', 'middleware' => 'throttle:40,5'], function () {

    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login')->name('login.post');

    Route::get('register', 'LoginController@showRegisterForm')->name('register');
    Route::post('register', 'LoginController@register')->name('register.post');

    Route::get('forgot/password', 'LoginController@showForgotPasswordForm')->name('forgot.password');
    Route::get('reset/password', 'LoginController@showResetPasswordForm')->name('reset.password');

    Route::get('logout', 'LoginController@logout')->name('logout');
    Route::get('register/final', 'LoginController@registration')->name('registration');
    Route::get('password/reset/', 'LoginController@showResetForm')->name('reset.password');
    Route::post('password/reset/check', 'LoginController@resetCheck')->name('reset.password.check');
    Route::get('password/reset/final', 'LoginController@resetFinal')->name('reset.password.final');
    Route::post('password/reset/final', 'LoginController@resetPassword')->name('reset.password.post');


});

//Route::get('/home', 'HomeController@index')->name('home');
