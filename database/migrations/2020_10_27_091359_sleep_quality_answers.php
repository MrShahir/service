<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SleepQualityAnswers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sleepQuality_answers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('answer');
            $table->string('amount')->default('0');
            $table->unsignedInteger('question_id');
            $table->unsignedInteger('priority');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sleepQuality_answers');
    }
}
