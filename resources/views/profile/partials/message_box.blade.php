{{-- for show messages --}}
@if(session('message'))
    <div class="alert alert-success alert-dismissible fade show" id="message_error_box" role="alert" style="margin: 10px !important;display: block;z-index: 9999 !important;">
        {{ session('message') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif