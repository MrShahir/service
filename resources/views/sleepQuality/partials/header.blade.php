<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Magnifica Questionnaire Form Wizard includes Coronavirus Health questionnaire">
    <meta name="author" content="Ansonika">
    <title>پیژامه | تست پایش کیفیت خواب </title>

    <!-- Favicons-->
    <link rel="shortcut icon" href="/sleepQuality/img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="/sleepQuality/img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="/sleepQuality/img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114"
          href="/sleepQuality/img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144"
          href="/sleepQuality/img/apple-touch-icon-144x144-precomposed.png">

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="/sleepQuality/css/bootstrap.min.css" rel="stylesheet">
    <link href="/sleepQuality/css/menu.css" rel="stylesheet">
    <link href="/sleepQuality/css/style.css" rel="stylesheet">
    <link href="/sleepQuality/css/vendors.css" rel="stylesheet">

    <!-- YOUR CUSTOM CSS -->
    <link href="/sleepQuality/css/custom.css" rel="stylesheet">

    <!-- MODERNIZR MENU -->
    <script src="/sleepQuality/js/modernizr.js"></script>

</head>

<body>

<div id="preloader">
    <div data-loader="circle-side"></div>
</div><!-- /Preload -->

<div id="loader_form">
    <div data-loader="circle-side-2"></div>
</div><!-- /loader_form -->

<header>
    <div class="container">
        <div class="row">
            <div class="col-3">
                <a href="index.html"><img src="/sleepQuality/img/logo.png" alt="" width="178" height="60" class="d-none d-md-block"><img src="/sleepQuality/img/logo.png" alt="" width="62" height="60" class="d-block d-md-none"></a>
            </div>
            <!--            <div class="col-9">-->
            <!--                <div id="social">-->
            <!--                    <ul>-->
            <!--                        <li><a href="#0"><i class="icon-facebook"></i></a></li>-->
            <!--                        <li><a href="#0"><i class="icon-twitter"></i></a></li>-->
            <!--                        <li><a href="#0"><i class="icon-google"></i></a></li>-->
            <!--                        <li><a href="#0"><i class="icon-linkedin"></i></a></li>-->
            <!--                    </ul>-->
            <!--                </div>-->
            <!--                &lt;!&ndash; /social &ndash;&gt;-->
            <!--                <a href="#0" class="cd-nav-trigger">Menu<span class="cd-icon"></span></a>-->
            <!--                &lt;!&ndash; /menu button &ndash;&gt;-->
            <!--                <nav>-->
            <!--                    <ul class="cd-primary-nav">-->
            <!--                        <li><a href="index.html" class="animated_link">Questionnaire</a></li>-->
            <!--                        <li><a href="prevention.html" class="animated_link">Prevention Tips</a></li>-->
            <!--                        <li><a href="faq.html" class="animated_link">Faq</a></li>-->
            <!--                        <li><a href="contacts.html" class="animated_link">Contact Us</a></li>-->
            <!--                        <li><a href="#0" class="animated_link">Purchase Template</a></li>-->
            <!--                    </ul>-->
            <!--                </nav>-->
            <!--                &lt;!&ndash; /menu &ndash;&gt;-->
            <!--            </div>-->
        </div>
    </div>
    <!-- /container -->
</header>
<!-- /Header -->