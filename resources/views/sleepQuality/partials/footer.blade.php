<div class="container">
    <footer id="home" class="clearfix">
        <p>© 2020 پیژامه - دانشنامه تخصصی خواب و استراحت</p>
        <ul>
            <li><a href="#0" class="animated_link">تست پایش کیفیت خواب</a></li>
            <li><a href="faq.html" class="animated_link">سوالات متداول</a></li>
            <li><a href="prevention.html" class="animated_link">از خواب چه میدانید؟</a></li>
        </ul>
    </footer>
    <!-- end footer-->
</div>
<!-- /container -->

<!-- COMMON SCRIPTS -->
<script src="/sleepQuality/js/jquery-3.2.1.min.js"></script>
<script src="/sleepQuality/js/common_scripts.min.js"></script>
<script src="/sleepQuality/js/velocity.min.js"></script>
<script src="/sleepQuality/js/common_functions.js"></script>

<!-- Wizard script-->
<script src="/sleepQuality/js/func_1.js"></script>

</body>
</html>