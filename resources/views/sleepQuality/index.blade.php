@include('sleepQuality.partials.header')

<div class="container">
    <div id="form_container">
        <div class="row no-gutters">
            <div class="col-lg-4">
                <div id="left_form">
                    <figure><img src="img/sleepicon.png" alt="" width="100" height="100"></figure>
                    <h2>نمره خوابت چنده؟<span>تست پایش کیفیت خواب</span></h2>
                    <p>این تست جهت سنجش کیفیت خواب شما توسط تیم پزشکی و روانشناسی پیژامه طراحی و تنظیم شده است و به شما
                        کمک خواهد کرد تا بتوانید کیفیت خواب خود را ارزیابی کنید، عادات غلط خود را بشناسید و در نهایت با
                        اصلاح عادات غلط بتوانید بهتر بخوابید.</p>
                    <a href="" class="btn_1 rounded yellow purchase" target="_parent">برای خواب بهتر شروع کن</a>
                    <a href="#wizard_container" class="btn_1 rounded mobile_btn yellow">شروع کن!</a>
                    <a href="#0" id="more_info" data-toggle="modal" data-target="#more-info"><i class="pe-7s-info"></i></a>
                </div>
            </div>
            <div class="col-lg-8">
                <div id="wizard_container">
                    <div id="top-wizard">
                        <div id="progressbar"></div>
                        <span id="location"></span>
                    </div>
                    <!-- /top-wizard -->
                    <form action="{{ route('testStore') }}" id="wrapped" method="post">
                        @csrf
                        <input id="website" name="website" type="text" value="">
                        <!-- Leave for security protection, read docs for details -->
                        <div id="middle-wizard">

                            <div class="step">
                                <h3 class="main_question"><i class="arrow_left"></i>چه هدفی از انجام این تست دارید؟</h3>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="container_check version_2">مدیریت هیجان و استرس
                                                <input type="checkbox" name="question_1[]" value="1-1"
                                                       class="required">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label class="container_check version_2">خواب بهتر
                                                <input type="checkbox" name="question_1[]" value="1-2"
                                                       class="required">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label class="container_check version_2">بهبود عملکرد و افزایش بازدهی
                                                <input type="checkbox" name="question_1[]"
                                                       value="1-3" class="required">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="container_check version_2">افزایش تمرکز
                                                <input type="checkbox" name="question_1[]" value="1-4"
                                                       class="required">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label class="container_check version_2">از روی کنجکاوی
                                                <input type="checkbox" name="question_1[]" value="1-5"
                                                       class="required">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <!-- /row -->
                            </div>
                            <!-- /step-->

{{--                            <div class="step">--}}
{{--                                <h3 class="main_question"><i class="arrow_left"></i>تناسب اندام شما چگونه است؟ به--}}
{{--                                    نظرم...</h3>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="container_radio version_2">لاغر هستم--}}
{{--                                        <input type="radio" name="question_2" value="5-5" class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_radio version_2">وزن ایده عالی دارم--}}
{{--                                        <input type="radio" name="question_2" value="5-2"--}}
{{--                                               class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_radio version_2">کمی اضافه وزن دارم--}}
{{--                                        <input type="radio" name="question_2" value="5-3"--}}
{{--                                               class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_radio version_2">خیلی اضافه وزن دارم--}}
{{--                                        <input type="radio" name="question_2" value="5-4"--}}
{{--                                               class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <!-- /step-->--}}

{{--                            <!-- /Start Branch ============================== -->--}}
{{--                            <div class="step" data-state="branchtype">--}}
{{--                                <label class="custom add_top_10"></label>--}}
{{--                                <h3 class="main_question"><i class="arrow_left"></i>جنسیت شما؟</h3>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="container_radio version_2">زن--}}
{{--                                        <input type="radio" name="gender" value="Yes" class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_radio version_2">مرد--}}
{{--                                        <input type="radio" name="gender" value="No" class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <!-- /if gender women > Yes ============================== -->--}}
{{--                            <div class="branch" id="Yes">--}}
{{--                                <div class="step" data-state="women111">--}}
{{--                                    <h3 class="main_question"><i class="arrow_left"></i>پیش از قاعدگی از کدامیک از عوامل زیر رنج می‌برید</h3>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label class="container_check version_2">درد و گرفتگی عضلانی--}}
{{--                                            <input type="checkbox" name="question_4[]" value="2-1"--}}
{{--                                                   class="required">--}}
{{--                                            <span class="checkmark"></span>--}}
{{--                                        </label>--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label class="container_check version_2">بیخوابی--}}
{{--                                            <input type="checkbox" name="question_4[]" value="2-2" class="required">--}}
{{--                                            <span class="checkmark"></span>--}}
{{--                                        </label>--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label class="container_check version_2">استرس و افسردگی--}}
{{--                                            <input type="checkbox" name="question_4[]" value="2-3"--}}
{{--                                                   class="required">--}}
{{--                                            <span class="checkmark"></span>--}}
{{--                                        </label>--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label class="container_check version_2">پرخوابی--}}
{{--                                            <input type="checkbox" name="question_4[]" value="2-4" class="required">--}}
{{--                                            <span class="checkmark"></span>--}}
{{--                                        </label>--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label class="container_check version_2">هیچکدام--}}
{{--                                            <input type="checkbox" name="question_4[]" value="2-5" class="required">--}}
{{--                                            <span class="checkmark"></span>--}}
{{--                                        </label>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="branch" id="women2">--}}
{{--                                <div class="step" data-state="women211">--}}
{{--                                    <h3 class="main_question"><i class="arrow_left"></i>در دوران شیردهی و حاملگی قرار--}}
{{--                                        دارید؟ </h3>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label class="container_radio version_2">بله--}}
{{--                                            <input type="radio" name="question_4_1[]"--}}
{{--                                                   value="3-1" class="required">--}}
{{--                                            <span class="checkmark"></span>--}}
{{--                                        </label>--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label class="container_radio version_2">خیر--}}
{{--                                            <input type="radio" name="question_4_1[]"--}}
{{--                                                   value="3-2" class="required">--}}
{{--                                            <span class="checkmark"></span>--}}
{{--                                        </label>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="branch" id="women3">--}}
{{--                                <div class="step" data-state="women311">--}}
{{--                                    <h3 class="main_question"><i class="arrow_left"></i>آیا در دوران یائسگی قرار دارید؟</h3>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label class="container_radio version_2">بله--}}
{{--                                            <input type="radio" name="question_4_2[]"--}}
{{--                                                   value="4-1" class="required">--}}
{{--                                            <span class="checkmark"></span>--}}
{{--                                        </label>--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label class="container_radio version_2">خیر--}}
{{--                                            <input type="radio" name="question_4_2[]"--}}
{{--                                                   value="4-2" class="required">--}}
{{--                                            <span class="checkmark"></span>--}}
{{--                                        </label>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <!-- /if gender men > No ============================== -->--}}
{{--                            <div class="step" id="No">--}}
{{--                                <h3 class="main_question"><i class="arrow_left"></i>به طور میانگین چند ساعت در طول--}}
{{--                                    شبانه روز می‌خوابید؟</h3>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="container_radio version_2">5 الی 6 ساعت--}}
{{--                                        <input type="radio" name="question_5" value="6-1"--}}
{{--                                               class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_radio version_2">6 الی 7 ساعت--}}
{{--                                        <input type="radio" name="question_5" value="6-2"--}}
{{--                                               class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_radio version_2">7 الی 8 ساعت--}}
{{--                                        <input type="radio" name="question_5" value="6-3"--}}
{{--                                               class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_radio version_2">8 الی 9 ساعت--}}
{{--                                        <input type="radio" name="question_5" value="6-4"--}}
{{--                                               class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <!-- /step-->--}}

{{--                            <div class="step">--}}
{{--                                <h3 class="main_question"><i class="arrow_left"></i>آیا هر شب در ساعت مشخصی--}}
{{--                                    میخوابید؟</h3>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="container_radio version_2">بله--}}
{{--                                        <input type="radio" name="question_6" value="7-1" class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_radio version_2">خیر--}}
{{--                                        <input type="radio" name="question_6" value="7-2" class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <!-- /step-->--}}

{{--                            <div class="step">--}}
{{--                                <h3 class="main_question"><i class="arrow_left"></i>زمانی که به جای خواب خود--}}
{{--                                    می‌روید، چقدر طول می‌کشد تا بخوابید</h3>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="container_radio version_2">کمتر از 15 دقیقه--}}
{{--                                        <input type="radio" name="question_7" value="8-1"--}}
{{--                                               class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_radio version_2"> بین 15 تا 30 دقیقه--}}
{{--                                        <input type="radio" name="question_7" value="8-2"--}}
{{--                                               class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_radio version_2"> بیشتر از 30 دقیقه--}}
{{--                                        <input type="radio" name="question_7" value="8-3"--}}
{{--                                               class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}


{{--                            <!-- /step-->--}}

{{--                            <div class="step">--}}
{{--                                <h3 class="main_question"><i class="arrow_left"></i>معمولا در طول شب، از خواب بیدار--}}
{{--                                    می‌شوید؟ </h3>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="container_radio version_2">بله--}}
{{--                                        <input type="radio" name="question_8" value="9-1" class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_radio version_2">خیر--}}
{{--                                        <input type="radio" name="question_8" value="9-2" class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <!-- /step-->--}}

{{--                            <div class="step">--}}
{{--                                <h3 class="main_question"><i class="arrow_left"></i>زمانی که از خواب بیدار می‌شوید--}}
{{--                                    سرحال و شاداب هستید؟</h3>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="container_radio version_2">بله--}}
{{--                                        <input type="radio" name="question_9" value="10-1" class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_radio version_2">خیر--}}
{{--                                        <input type="radio" name="question_9" value="10-2" class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <!-- /step-->--}}

{{--                            <div class="step">--}}
{{--                                <h3 class="main_question"><i class="arrow_left"></i>آیا در طول روز چرت می‌زنید؟--}}
{{--                                </h3>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="container_radio version_2">بله--}}
{{--                                        <input type="radio" name="question_10" value="11-1" class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_radio version_2">خیر--}}
{{--                                        <input type="radio" name="question_10" value="11-2" class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <!-- /step-->--}}

{{--                            <div class="step">--}}
{{--                                <h3 class="main_question"><i class="arrow_left"></i>برای بیدار شدن از خواب، زنگ ساعت--}}
{{--                                    خود را به تاخیر می‌اندازید؟</h3>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="container_radio version_2">بله--}}
{{--                                        <input type="radio" name="question_11" value="12-1" class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_radio version_2">خیر--}}
{{--                                        <input type="radio" name="question_11" value="12-2" class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <!-- /step-->--}}

{{--                            <div class="step">--}}
{{--                                <h3 class="main_question"><i class="arrow_left"></i>در طول خواب خروپف می‌کنید؟ </h3>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="container_radio version_2">بله--}}
{{--                                        <input type="radio" name="question_12" value="13-1" class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_radio version_2">خیر--}}
{{--                                        <input type="radio" name="question_12" value="13-2" class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <!-- /step-->--}}

{{--                            <div class="step">--}}
{{--                                <h3 class="main_question"><i class="arrow_left"></i>کدام یک از موارد زیر را چند ساعت قبل از خواب مصرف می‌کنید؟</h3>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="container_check version_2">قهوه (کافئین)--}}
{{--                                        <input type="checkbox" name="question_13[]" value="14-1"--}}
{{--                                               class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_check version_2">چای (تئین)--}}
{{--                                        <input type="checkbox" name="question_13[]" value="14-2"--}}
{{--                                               class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_check version_2">الکل--}}
{{--                                        <input type="checkbox" name="question_13[]" value="14-3" class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_check version_2">سیگار--}}
{{--                                        <input type="checkbox" name="question_13[]" value="14-4" class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                                <small>چند مورد را میتوانید انتخاب کنید.</small>--}}
{{--                            </div>--}}
{{--                            <!-- /step-->--}}

{{--                            <div class="step">--}}
{{--                                <h3 class="main_question"><i class="arrow_left"></i>آیا قبل از خواب فعالیت بدنی--}}
{{--                                    سنگین دارید؟</h3>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="container_radio version_2">بله--}}
{{--                                        <input type="radio" name="question_14" value="15-1" class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_radio version_2">خیر--}}
{{--                                        <input type="radio" name="question_14" value="15-2" class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}

{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <!-- /step-->--}}
{{--                            <div class="step">--}}
{{--                                <h3 class="main_question"><i class="arrow_left"></i>آیا قبل از خواب در معرض نور آبی--}}
{{--                                    صفحه نمایش (موبایل – تبلت – کامپیوتر – تلویزیون و ... ) قرار دارید؟</h3>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="container_radio version_2">بله--}}
{{--                                        <input type="radio" name="question_15" value="16-1" class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_radio version_2">خیر--}}
{{--                                        <input type="radio" name="question_15" value="16-2" class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <!-- /step-->--}}
{{--                            <div class="step">--}}
{{--                                <h3 class="main_question"><i class="arrow_left"></i>آخرین وعده غذایی خود را چند ساعت--}}
{{--                                    قبل از خواب میل می‌کنید؟</h3>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="container_radio version_2">کمتر از 2 ساعت--}}
{{--                                        <input type="radio" name="question_16" value="17-1"--}}
{{--                                               class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_radio version_2">2 الی 4 ساعت--}}
{{--                                        <input type="radio" name="question_16" value="17-2"--}}
{{--                                               class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_radio version_2">4 الی 6 ساعت--}}
{{--                                        <input type="radio" name="question_16" value="17-3"--}}
{{--                                               class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_radio version_2">بیشتر از 6 ساعت--}}
{{--                                        <input type="radio" name="question_16" value="17-4"--}}
{{--                                               class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <!-- /step-->--}}

{{--                            <div class="step">--}}
{{--                                <h3 class="main_question"><i class="arrow_left"></i>در رختخواب خود به جز خوابیدن،--}}
{{--                                    کدامیک از موارد زیر را انجام می‌دهید؟</h3>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="container_check version_2">غذا خوردن--}}
{{--                                        <input type="checkbox" name="question_17[]" value="18-1" class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_check version_2">2درس خواندن و مطالعه--}}
{{--                                        <input type="checkbox" name="question_17[]" value="18-2"--}}
{{--                                               class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_check version_2">کار با لوازم دیجیتال--}}
{{--                                        <input type="checkbox" name="question_17[]" value="18-3"--}}
{{--                                               class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <!-- /step-->--}}

{{--                            <div class="step">--}}
{{--                                <h3 class="main_question"><i class="arrow_left"></i>از چه لوازمی برای راحت‌تر--}}
{{--                                    خوابیدن استفاده می‌کنید؟</h3>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="container_check version_2">چشم بند--}}
{{--                                        <input type="checkbox" name="question_18[]" value="19-1" class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_check version_2">بالش بین زانو--}}
{{--                                        <input type="checkbox" name="question_18[]" value="19-2"--}}
{{--                                               class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_check version_2">تشک طبی--}}
{{--                                        <input type="checkbox" name="question_18[]" value="19-3" class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_check version_2">بالش طبی--}}
{{--                                        <input type="checkbox" name="question_18[]" value="19-4" class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_check version_2">هیچکدام--}}
{{--                                        <input type="checkbox" name="question_18[]" value="19-5" class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                                <small>چند مورد را میتوانید انتخاب کنید.</small>--}}
{{--                            </div>--}}
{{--                            <!-- /step-->--}}

{{--                            <div class="step">--}}
{{--                                <h3 class="main_question"><i class="arrow_left"></i>در چه بازه‌ای از روز بیشترین انرژی را دارید؟</h3>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="container_radio version_2">8 تا 11--}}
{{--                                        <input type="radio" name="question_19" value="20-1" class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_radio version_2">11 تا 16--}}
{{--                                        <input type="radio" name="question_19" value="20-2" class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_radio version_2">16 تا 20--}}
{{--                                        <input type="radio" name="question_19" value="20-3" class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_radio version_2">20 تا 24--}}
{{--                                        <input type="radio" name="question_19" value="20-4" class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_radio version_2">24 تا 8 صبح--}}
{{--                                        <input type="radio" name="question_19" value="20-5"--}}
{{--                                               class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <!-- /step-->--}}

{{--                            <div class="step">--}}
{{--                                <h3 class="main_question"><i class="arrow_left"></i>کدامیک  مورد یا موارد  زیر برای شما--}}
{{--                                    صدق می‌کند؟</h3>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="container_check version_2">زمان خوابیدن چراع اتاق یا یک منبع نوری--}}
{{--                                        در اتاق خواب روشن است.--}}
{{--                                        <input type="checkbox" name="question_20[]"--}}
{{--                                               value="21-1"--}}
{{--                                               class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_check version_2">در زمان خوابیدن صدایی در محل خواب شما--}}
{{--                                        وجود دارد.--}}
{{--                                        <input type="checkbox" name="question_20[]"--}}
{{--                                               value="21-2"--}}
{{--                                               class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_check version_2">برای بهتر خوابیدن از دمنوش‌های خواب آور--}}
{{--                                        استفاده میکنم--}}
{{--                                        <input type="checkbox" name="question_20[]"--}}
{{--                                               value="21-3"--}}
{{--                                               class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_check version_2">برای بهتر خوابیدن از قرص‌های خواب و--}}
{{--                                        آرام‌بخش استفاده میکنم--}}
{{--                                        <input type="checkbox" name="question_20[]"--}}
{{--                                               value="21-4"--}}
{{--                                               class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                    <label class="container_check version_2">قبل از خواب به موسیقی یا پادکست گوش--}}
{{--                                        میکنم--}}
{{--                                        <input type="checkbox" name="question_20[]"--}}
{{--                                               value="21-5" class="required">--}}
{{--                                        <span class="checkmark"></span>--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                                <small>چند مورد را میتوانید انتخاب کنید.</small>--}}
{{--                            </div>--}}
{{--                            <!-- /step-->--}}


{{--                            <div class="step">--}}
{{--                                <h3 class="main_question"><i class="arrow_left"></i>لطفا اطلاعات شخصیتان را پر کنید</h3>--}}
{{--                                <div class="form-group add_top_30">--}}
{{--                                    <label for="name">نام و نام خانوادگی</label>--}}
{{--                                    <input type="text" name="name" id="name" class="form-control required"--}}
{{--                                           onchange="getVals(this, 'name_field');">--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label for="email">آدرس ایمیل</label>--}}
{{--                                    <input type="email" name="email" id="email" class="form-control required"--}}
{{--                                           onchange="getVals(this, 'email_field');">--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label for="phone">شماره تماس</label>--}}
{{--                                    <input type="text" name="phone" id="phone" class="form-control">--}}
{{--                                </div>--}}
{{--                                <div class="row">--}}
{{--                                    <div class="col-lg-3 col-md-3 col-sm-3 col-4">--}}
{{--                                        <label for="age">سن</label>--}}
{{--                                        <div class="form-group radio_input">--}}
{{--                                            <input type="text" name="age" id="age" class="form-control required">--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <!-- /row-->--}}
{{--                            </div>--}}

                            <div class="submit step" id="end">
                                <div class="summary">
                                    <div class="wrapper">
                                        <h3>با تشکر از زمانی که برای خودتان گذاشتید<br><span id="name_field"></span>!</h3>
                                        <p>جواب تست را ما به زودی به ایمیلتون می فرستیم<strong
                                                    id="email_field"></strong> اگر جواب تست شما نگران کننده بود، نگران نباشید، با راه کارهایی که پیش روی شما قرار میدیم می توانید خود درمانی را آغاز کنید، اگر اوضاع وخیم بود پزشکان متخصص پیژامه با کمال میل در خدمت شما هستند.</p>
                                    </div>
                                    <div class="text-center">
                                        <div class="form-group terms">
                                            <label class="container_check">اطلاعات<a href="#"
                                                                                     data-toggle="modal"
                                                                                     data-target="#terms-txt">فرمی که پر کرده ام را</a> قبول دارم
                                                <input type="checkbox" name="terms" value="Yes" class="required">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /step last-->

                        </div>
                        <!-- /middle-wizard -->
                        <div id="bottom-wizard">
                            <button type="button" name="backward" class="backward">قبلی</button>
                            <button type="button" name="forward" class="forward">بعدی</button>
                            <button type="submit" name="process" class="submit">ثبت</button>
                        </div>
                        <!-- /bottom-wizard -->
                    </form>
                </div>
                <!-- /Wizard container -->
            </div>
        </div><!-- /Row -->
    </div><!-- /Form_container -->
</div>
<!-- /container -->

<div class="cd-overlay-nav">
    <span></span>
</div>
<!-- /cd-overlay-nav -->
<div class="cd-overlay-content">
    <span></span>
</div>
<!-- /cd-overlay-content -->

<!-- Modal terms -->
<div class="modal fade" id="terms-txt" tabindex="-1" role="dialog" aria-labelledby="termsLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="termsLabel">قوانین و مقررات</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <p>Lorem ipsum dolor sit amet, in porro albucius qui, in <strong>nec quod novum accumsan</strong>, mei
                    ludus tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum sanctus, pro ne
                    quod dicunt sensibus.</p>
                <p>Lorem ipsum dolor sit amet, in porro albucius qui, in nec quod novum accumsan, mei ludus tamquam
                    dolores id. No sit debitis meliore postulant, per ex prompta alterum sanctus, pro ne quod dicunt
                    sensibus. Lorem ipsum dolor sit amet, <strong>in porro albucius qui</strong>, in nec quod novum
                    accumsan, mei ludus tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum
                    sanctus, pro ne quod dicunt sensibus.</p>
                <p>Lorem ipsum dolor sit amet, in porro albucius qui, in nec quod novum accumsan, mei ludus tamquam
                    dolores id. No sit debitis meliore postulant, per ex prompta alterum sanctus, pro ne quod dicunt
                    sensibus.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn_1" data-dismiss="modal">بستن</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal info -->
<div class="modal fade" id="more-info" tabindex="-1" role="dialog" aria-labelledby="more-infoLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="more-infoLabel">سوالاتی که اکثرا می پرسند</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <p>Lorem ipsum dolor sit amet, in porro albucius qui, in <strong>nec quod novum accumsan</strong>, mei
                    ludus tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum sanctus, pro ne
                    quod dicunt sensibus.</p>
                <p>Lorem ipsum dolor sit amet, in porro albucius qui, in nec quod novum accumsan, mei ludus tamquam
                    dolores id. No sit debitis meliore postulant, per ex prompta alterum sanctus, pro ne quod dicunt
                    sensibus. Lorem ipsum dolor sit amet, <strong>in porro albucius qui</strong>, in nec quod novum
                    accumsan, mei ludus tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum
                    sanctus, pro ne quod dicunt sensibus.</p>
                <p>Lorem ipsum dolor sit amet, in porro albucius qui, in nec quod novum accumsan, mei ludus tamquam
                    dolores id. No sit debitis meliore postulant, per ex prompta alterum sanctus, pro ne quod dicunt
                    sensibus.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn_1" data-dismiss="modal">بستن</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

@include('sleepQuality.partials.footer')