<?php

namespace App\Http\Controllers\Auth;

use App\classes\Sendinblue;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('profile.login');
    }
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:6'
        ], [

            'email.required' => 'ایمیل خود را وارد کنید' ,
            'email.email' => 'ایمیل وارد شده نامعتبر است' ,
            'password.required' => 'لطفا رمز عبور خود را وارد کنید',
            'password.min' => 'رمز عبور باید حداقل ۶ کاراکتر باشد'
        ]);

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password,'is_verified' => true], true)) {
            return redirect()->route('profile')->with('message', 'شما با موفقیت وارد سایت شدید');
        } else {
            return redirect()->back()->with([
                'message' => 'ایمیل یا رمزعبور وارد شده اشتباه است'
            ]);
        }

        return redirect()->route('home');
    }

    public function showRegisterForm() {
        return view('profile.register');
    }

    public function register(Request $request)
    {
        $request->validate([
            'first_name' => 'required' ,
            'last_name' => 'required' ,
            'email' => 'required | email | unique:users,email' ,
            'password' => 'required|confirmed|min:6'
        ], [
            'first_name.required' => 'لطفا نام خود را وارد کنید' ,
            'last_name.required' => 'نام خانوادگی را وارد نمایید' ,
            'email.required' => 'ایمیل خود را وارد کنید' ,
            'email.email' => 'ایمیل وارد شده نامعتبر است' ,
            'email.unique' => 'این ایمیل قبلا در سایت وارد شده است' ,
            'password.required' => 'لطفا رمز عبور خود را وارد کنید',
            'password.min' => 'رمز عبور باید حداقل ۶ کاراکتر باشد',
            'password.confirmed' => 'رمز عبور با تاییده ان مطابقت ندارد'
        ]);

        $user = new User();

        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);

        $user->save();


//        Auth::login($user, true);

        return redirect()->route('profile')->with('message', 'شما با موفقیت ثبت نام کردید');
    }

    public function showForgotPasswordForm() {
        return view('profile.forgot_password');
    }

    public function showResetPasswordForm() {
        return view('profile.reset_password');
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('home')->with('message' , 'شما با موفقیت خارج شدید');
    }


    public function registration()
    {
        if (empty(session('code'))) {
            return redirect()->route('login')->with('message', 'اطلاعات وارد شده اشتباه است لطفا دوباره امتحان کنید');
        }
        return view('landing.login_register')->with('code', session('code'));
    }


    public function showResetForm()
    {
        return view('landing.reset_password');
    }

    public function resetCheck(Request $request)
    {
        $request->validate([
            'cellphone' => 'required|regex:/(09)[0-9]{9}/|digits:11|numeric',
        ], [
            'cellphone.regex' => 'فیلد شماره موبایل اشتباه است لطفا با ۰۹ شروع کنید',
            'cellphone.required' => 'لطفا شماره موبایل خود را وارد کنید'
        ]);

        $user = User::where([
            'cellphone' => $request->cellphone ,
            'active' => true
        ])->first();
        if (!empty($user)) {
            $verification_code = random_int(1000, 9999);
            dump('Send Sms Code' . $verification_code);
            return redirect()->route('reset.password.final')->with([
                'cellphone' => $request->cellphone,
                'code' => $verification_code
            ]);
        } else {
            return redirect()->route('login')->with('message', 'شما قبلا داخل سایت ثبت نام نکرده اید');
        }
    }

    public function resetFinal()
    {
        if (empty(session('code'))) {
            return redirect()->route('login')->with('message', 'اطلاعات وارد شده اشتباه است لطفا دوباره امتحان کنید');
        }
        return view('landing.reset_password')->with('code', session('code'));
    }

    public function resetPassword(Request $request)
    {
        if (empty(decrypt($request->code)) || !is_numeric(decrypt($request->code))) {
            return redirect()->route('login')->with('message', 'اطلاعات وارد شده اشتباه است لطفا دوباره امتحان کنید');
        }
        $request->validate([
            'cellphone' => 'required|regex:/(09)[0-9]{9}/|digits:11|numeric',
            'verification_code' => 'required|max:6',
            'password' => 'required|confirmed|min:6'
        ], [

            'cellphone.regex' => 'فیلد شماره موبایل اشتباه است لطفا با ۰۹ شروع کنید',
            'cellphone.required' => 'لطفا شماره موبایل خود را وارد کنید',
            'verification_code.required' => 'کد تایید ارسال شده را وارد کنید',
            'verification_code.max' => 'کد تایید وارد شده اشتباه است',
            'verification_code.numeric' => 'کد تایید وارد شده اشتباه است',
            'password.required' => 'لطفا رمز عبور خود را وارد کنید',
            'password.min' => 'رمز عبور باید حداقل ۶ کاراکتر باشد',
            'password.confirmed' => 'رمز عبور با تاییده ان مطابقت ندارد'
        ]);

        $user = User::where('cellphone', $request->cellphone)->first();
        if (!empty($user)) {
            $user->verification_code = decrypt($request->code);
            $user->password = Hash::make($request->password);

            $user->save();
        }else {
            return redirect()->route('login')->with('message', 'اطلاعات وارد شده اشتباه است لطفا دوباره امتحان کنید');
        }

        return redirect()->route('home')->with('message', 'رمز عبور شما با موفقیت تغییر کرد');
    }


}
