<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SleepQualityAnswer extends Model
{
    protected $table = 'sleepQuality_answers';

    protected $fillable = [
        'answer' ,
        'amount' ,
        'question_id' ,
        'priority'
    ];

    public function question() {
        return $this->belongsTo(SleepQualityQuestion::class , 'question_id');
    }
}
