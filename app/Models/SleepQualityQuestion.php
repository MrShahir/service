<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SleepQualityQuestion extends Model
{
    protected $table = 'sleepQuality_questions';

    protected $fillable = [
        'question' ,
        'gender'
    ];

    public function answers() {
        return $this->hasMany(SleepQualityAnswer::class , 'question_id');
    }
}
