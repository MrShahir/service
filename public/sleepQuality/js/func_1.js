/*  Wizard */
jQuery(function($) {
    "use strict";
    // $('form#wrapped').attr('action', 'send_email_1.php');
    $("#wizard_container").wizard({
        stepsWrapper: "#wrapped",
        submit: ".submit",
        unidirectional: false,
        beforeSelect: function(event, state) {
            if ($('input#website').val().length != 0) {
                return false;
            }
            if (!state.isMovingForward)
                return true;
            var inputs = $(this).wizard('state').step.find(':input');

            return !inputs.length || !!inputs.valid();
        }
    }).validate({
        errorPlacement: function(error, element) {
            if (element.is(':radio') || element.is(':checkbox')){
                console.log(1);
                error.insertBefore(element.next());
            } else {
                console.log(1);
                error.insertAfter(element);
            }
        }
    });
    //  progress bar
    $("#progressbar").progressbar();
    $("#wizard_container").wizard({
        afterSelect: function(event, state) {
            $("#progressbar").progressbar("value", state.percentComplete);
            $("#location").text("" + state.stepsComplete + " از " + state.stepsPossible + " پاسخ داده شده ");
        }
    });
});

$("#wizard_container").wizard({
    transitions: {
        branchtype: function($step, action) {
            var branch = $step.find(":checked").val();
            if (!branch) {
                $("form").valid();
            }
            return branch;
        },
        women111: function ($step, action) {
            var women2validation = $("form").valid();
            return "women2";
        },
        women211: function ($step, action) {
            var formValidation = $("form").valid();
            if(formValidation)
                return "women3";
        },
        women311: function ($step, action) {
            var formValidation = $("form").valid();
            if (formValidation)
                return "No";
        }
    }
});


// Input name and email value
function getVals(formControl, controlType) {
    switch (controlType) {

        case 'name_field':
            // Get the value for input
            var value = $(formControl).val();
            $("#name_field").text(value);
            break;

        case 'email_field':
            // Get the value for input
            var value = $(formControl).val();
            $("#email_field").text(value);
            break;
    }
}